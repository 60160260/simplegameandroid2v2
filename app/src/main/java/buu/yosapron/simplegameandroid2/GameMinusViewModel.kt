package buu.yosapron.simplegameandroid2

import android.graphics.Color
import android.util.Log
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class GameMinusViewModel : ViewModel() {

    private var _amountCorrect = MutableLiveData<Int>()
    val amountCorrect: LiveData<Int>
        get() = _amountCorrect

    private var _amountIncorrect = MutableLiveData<Int>()
    val amountIncorrect: LiveData<Int>
        get() = _amountIncorrect

    private var _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private var _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private var _ans = MutableLiveData<Int>()
    val ans: LiveData<Int>
        get() = _ans

    private var _textSelect = MutableLiveData<String>()
    val textSelect: LiveData<String>
        get() = _textSelect

    private var _btnChoose1 = MutableLiveData<Int>()
    val btnChoose1: LiveData<Int>
        get() = _btnChoose1

    private var _btnChoose2 = MutableLiveData<Int>()
    val btnChoose2: LiveData<Int>
        get() = _btnChoose2

    private var _btnChoose3 = MutableLiveData<Int>()
    val btnChoose3: LiveData<Int>
        get() = _btnChoose3

    private var _eventChoose1 = MutableLiveData<Boolean>()
    val eventChoose1: LiveData<Boolean>
        get() = _eventChoose1

    private var _eventChoose2 = MutableLiveData<Boolean>()
    val eventChoose2: LiveData<Boolean>
        get() = _eventChoose2

    private var _eventChoose3 = MutableLiveData<Boolean>()
    val eventChoose3: LiveData<Boolean>
        get() = _eventChoose3

    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish: LiveData<Boolean>
        get() = _eventGameFinish


    init {
        _amountCorrect.value = 0
        _amountIncorrect.value = 0
        _textSelect.value = "เลือกข้อที่ถูกต้อง"
        play()
        Log.i("GameMinusViewModel", "GameMinusViewModel created!")
    }

    fun addAmountCorrect() {
        _amountCorrect.value = _amountCorrect.value?.plus(1)
    }

    fun addAmountIncorrect() {
        _amountIncorrect.value = _amountIncorrect.value?.plus(1)
    }

    fun setScore(amountCorrect: Int, amountIncorrect: Int) {
        _amountCorrect.value = amountCorrect
        _amountIncorrect.value = amountIncorrect
    }

    fun randomNumAndSetText(): Int {
        val randomNum1 = Random.nextInt(1, 10)
        val randomNum2 = Random.nextInt(1, 10)
        val randomPattern = Random.nextInt(1, 5)
        _num1.value = randomNum1
        _num2.value = randomNum2
        _ans.value = randomNum1 - randomNum2
        return randomPattern
    }

    fun onChoose1() {
        if (_btnChoose1.value == _ans.value) {
            addAmountCorrect()
        } else {
            addAmountIncorrect()
        }
        _eventChoose1.value = true
    }

    fun onChoose2() {
        if (_btnChoose2.value == _ans.value) {
            addAmountCorrect()
        } else {
            addAmountIncorrect()
        }
        _eventChoose2.value = true
    }
    fun onChoose3() {
        if (_btnChoose3.value == _ans.value) {
            addAmountCorrect()
        } else {
            addAmountIncorrect()
        }
        _eventChoose3.value = true
    }

    fun setTextSelect(textSelect: String) {
        _textSelect.value = textSelect
    }

    fun play() {

        val randomPattern = randomNumAndSetText()
        if (randomPattern == 1) {
            _btnChoose1.value = _ans.value
            _btnChoose2.value = _ans.value?.minus(1)
            _btnChoose3.value = _ans.value?.plus(1)


        } else if (randomPattern == 2) {

            _btnChoose1.value = _ans.value?.plus(1)
            _btnChoose2.value = _ans.value
            _btnChoose3.value = _ans.value?.minus(1)


        } else if (randomPattern == 3) {

            _btnChoose1.value = _ans.value?.minus(2)
            _btnChoose2.value = _ans.value?.plus(2)
            _btnChoose3.value = _ans.value

        } else if (randomPattern == 4) {

            _btnChoose1.value = _ans.value
            _btnChoose2.value = _ans.value?.minus(2)
            _btnChoose3.value = _ans.value?.plus(2)


        } else {
            _btnChoose1.value = _ans.value?.plus(1)
            _btnChoose2.value = _ans.value
            _btnChoose3.value = _ans.value?.minus(2)


        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GamePlusViewModel", "GamePlusViewModel destroyed!")
    }


}