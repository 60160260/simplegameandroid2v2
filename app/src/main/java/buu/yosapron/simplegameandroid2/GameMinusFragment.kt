package buu.yosapron.simplegameandroid2

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.yosapron.simplegameandroid2.databinding.FragmentGameMinusBinding
import buu.yosapron.simplegameandroid2.databinding.FragmentGamePlusBinding
import kotlinx.android.synthetic.main.fragment_game_plus.*

class GameMinusFragment : Fragment() {
    private lateinit var binding: FragmentGameMinusBinding

    private lateinit var viewModel: GameMinusViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game_minus, container, false)
        Log.i("GamePlusFragment", "Call ViewModelProvider.of")
        viewModel = ViewModelProvider(this).get(GameMinusViewModel::class.java)
        binding.gameMinusViewModel = viewModel

        val args = GameMinusFragmentArgs.fromBundle(arguments!!)

        viewModel.setScore(args.amountCorrect, args.amountIncorrect)

        binding.btnBack.setOnClickListener { onBackGame() }

        viewModel.eventChoose1.observe(this, Observer {
            if (it) {

                if (viewModel.btnChoose1.value == viewModel.ans.value) {
                    viewModel.setTextSelect(getString(R.string.textCorrect))
                    viewModel.play()

                    btnChoose1.setBackgroundColor(Color.GRAY)
                    btnChoose2.setBackgroundColor(Color.GRAY)
                    btnChoose3.setBackgroundColor(Color.GRAY)

                    btnChoose1.isEnabled = true
                    btnChoose2.isEnabled = true
                    btnChoose3.isEnabled = true

                } else {
                    viewModel.setTextSelect(getString(R.string.textIncorrect))
                    btnChoose1.setBackgroundColor(Color.RED)
                    btnChoose1.isEnabled = false
                }
                binding.invalidateAll()
            }
        })

        viewModel.eventChoose2.observe(this, Observer {
            if (it) {
                if (viewModel.btnChoose2.value == viewModel.ans.value) {
                    viewModel.setTextSelect(getString(R.string.textCorrect))
                    viewModel.play()

                    btnChoose1.setBackgroundColor(Color.GRAY)
                    btnChoose2.setBackgroundColor(Color.GRAY)
                    btnChoose3.setBackgroundColor(Color.GRAY)

                    btnChoose1.isEnabled = true
                    btnChoose2.isEnabled = true
                    btnChoose3.isEnabled = true

                } else {
                    viewModel.setTextSelect(getString(R.string.textIncorrect))
                    btnChoose2.setBackgroundColor(Color.RED)
                    btnChoose2.isEnabled = false
                }
                binding.invalidateAll()
            }
        })

        viewModel.eventChoose3.observe(this, Observer {
            if (it) {
                if (viewModel.btnChoose3.value == viewModel.ans.value) {
                    viewModel.setTextSelect(getString(R.string.textCorrect))
                    viewModel.play()

                    btnChoose1.setBackgroundColor(Color.GRAY)
                    btnChoose2.setBackgroundColor(Color.GRAY)
                    btnChoose3.setBackgroundColor(Color.GRAY)

                    btnChoose1.isEnabled = true
                    btnChoose2.isEnabled = true
                    btnChoose3.isEnabled = true

                } else {
                    viewModel.setTextSelect(getString(R.string.textIncorrect))
                    btnChoose3.setBackgroundColor(Color.RED)
                    btnChoose3.isEnabled = false
                }
                binding.invalidateAll()
            }
        })

        return binding.root
    }

    private fun onBackGame() {
        Toast.makeText(activity, "Game has just back to menu", Toast.LENGTH_LONG).show()
        val action = GameMinusFragmentDirections.actionGameMinusFragmentToTitleFragment()
        action.amountCorrect = viewModel.amountCorrect.value!!
        action.amountIncorrect = viewModel.amountIncorrect.value!!
        view?.findNavController()?.navigate(action)
    }


}